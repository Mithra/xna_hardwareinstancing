using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace HardwareInstancing
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        public static readonly VertexDeclaration HI_InstanceVertexDeclaration = new VertexDeclaration(
            new VertexElement(0, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 0),
            new VertexElement(16, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 1),
            new VertexElement(32, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 2),
            new VertexElement(48, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 3),
            new VertexElement(64, VertexElementFormat.Vector4, VertexElementUsage.TextureCoordinate, 1));

        GraphicsDeviceManager graphics;
        
        private SceneModel _model;
        private Effect _effect;

        // Camera
        private Matrix _world;
        private Matrix _view;
        private Matrix _projection;
        private Matrix _worldViewProj;
        private Vector3 _target;
        private Vector3 _lookDir;
        private Vector3 _up;
        private float _distance;
        private float _angle;

        // HI Stuff
        private DynamicVertexBuffer _buffer;
        private VertexBufferBinding[] _vertexBufferBindings;
        private ModelInstance[] _instances;
        private const int nbModels = 5000;
        private bool _hiEnabled = true;

        // FPS
        private int _framecount;
        private float _timeSinceLastUpdate;
        private const float UpdateInterval = 0.5f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            _model = new SceneModel("cube");
            _model.LoadContent(Content);

            _effect = Content.Load<Effect>("effect");

            // Camera
            _world = Matrix.Identity;
            _view = Matrix.CreateLookAt(new Vector3(0, 0, -20), Vector3.Zero, Vector3.Up);
            _projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, 4f / 3f, 1.0f, 1000f);

            _distance = 100;
            _target = new Vector3(0, 0, 0);
            _lookDir = new Vector3(0, 35, -60);
            _lookDir.Normalize();
            _up = Vector3.Up;
            _angle = 0;

            _instances = new ModelInstance[nbModels];

            Random rnd = new Random();

            for (int i = 0; i < nbModels; i++)
            {
                _instances[i].World = Matrix.CreateTranslation(rnd.Next(-20, 20), rnd.Next(-20, 20), rnd.Next(-20, 20));
                _instances[i].Color = new Vector4(rnd.Next(0, 255) / 255f, rnd.Next(0, 255) / 255f, rnd.Next(0, 255) / 255f, 1);
            }

            _buffer = new DynamicVertexBuffer(GraphicsDevice, HI_InstanceVertexDeclaration, nbModels, BufferUsage.WriteOnly);
            _buffer.SetData(_instances, 0, nbModels, SetDataOptions.Discard);

            _vertexBufferBindings = new VertexBufferBinding[2];

            GraphicsDevice.RasterizerState = new RasterizerState
                                                 {
                                                     CullMode = CullMode.CullClockwiseFace
                                                 };


            foreach (ModelMeshPart part in _model.Model.Meshes.SelectMany(mesh => mesh.MeshParts))
            {
                part.Effect = _effect;
            }

            IsFixedTimeStep = false;
            graphics.SynchronizeWithVerticalRetrace = false;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            _hiEnabled = !Keyboard.GetState().IsKeyDown(Keys.Enter);                

            float elapsed = (float) gameTime.ElapsedGameTime.TotalSeconds;

            _framecount++;
            _timeSinceLastUpdate += elapsed;

            if (_timeSinceLastUpdate > UpdateInterval)
            {
                float fps = _framecount / _timeSinceLastUpdate;
                _framecount = 0;
                _timeSinceLastUpdate -= UpdateInterval;

                Window.Title = string.Format("Hardware Instancing {0} - {1} FPS", _hiEnabled ? "ON" : "OFF", fps);
            }

            _angle += elapsed;
            _angle %= MathHelper.TwoPi;

            Quaternion rot = Quaternion.CreateFromYawPitchRoll(_angle, 0, 0);
            Vector3 position = _lookDir * _distance;
            Vector3.Transform(ref position, ref rot, out position);

            Matrix.CreateLookAt(ref position, ref _target, ref _up, out _view);

            Matrix.Multiply(ref _world, ref _view, out _worldViewProj);
            Matrix.Multiply(ref _worldViewProj, ref _projection, out _worldViewProj);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _effect.Parameters["WorldViewProjection"].SetValue(_worldViewProj);

            if (_hiEnabled)
                DrawHI();
            else
                DrawBasic();

            base.Draw(gameTime);
        }

        public void DrawBasic()
        {
            GraphicsDevice.SetVertexBuffer(null);

            _effect.CurrentTechnique = _effect.Techniques["Default"];

            for (int i = 0; i < nbModels; i++)
            {
                foreach (ModelMesh modelMesh in _model.Model.Meshes)
                {
                    _effect.Parameters["World"].SetValue(_model._transforms[modelMesh.ParentBone.Index] * _instances[i].World);
                    modelMesh.Draw();
                }
            }
        }

        public void DrawHI()
        {
            GraphicsDevice.SetVertexBuffer(null);

            _effect.CurrentTechnique = _effect.Techniques["HI"];

            foreach (ModelMesh modelMesh in _model.Model.Meshes)
            {
                foreach (ModelMeshPart meshPart in modelMesh.MeshParts)
                {
                    _vertexBufferBindings[0] = new VertexBufferBinding(meshPart.VertexBuffer, meshPart.VertexOffset, 0);
                    _vertexBufferBindings[1] = new VertexBufferBinding(_buffer, 0, 1);

                    GraphicsDevice.SetVertexBuffers(_vertexBufferBindings);
                    GraphicsDevice.Indices = meshPart.IndexBuffer;

                    foreach (EffectPass pass in _effect.CurrentTechnique.Passes)
                    {
                        pass.Apply();

                        GraphicsDevice.DrawInstancedPrimitives(PrimitiveType.TriangleList, 0, 0,
                                                               meshPart.NumVertices, meshPart.StartIndex,
                                                               meshPart.PrimitiveCount, nbModels);
                    }
                }
            }
        }
    }
}
