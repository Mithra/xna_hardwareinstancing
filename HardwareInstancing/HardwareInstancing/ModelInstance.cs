﻿using Microsoft.Xna.Framework;

namespace HardwareInstancing
{
    public struct ModelInstance
    {
        public Matrix World;
        public Vector4 Color;
    }
}
