﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace HardwareInstancing
{
    public class SceneModel
    {
        private readonly string _modelName;
        public Matrix[] _transforms;

        public Model Model
        {
            get; 
            private set;
        }
        
        public SceneModel(string modelName)
        {
            _modelName = modelName;
        }

        public void LoadContent(ContentManager content)
        {
            Model = content.Load<Model>(_modelName);

            _transforms = new Matrix[Model.Bones.Count];

            // Copy any parent transforms.
            Model.CopyAbsoluteBoneTransformsTo(_transforms);
        }
    }
}
