float4x4 World;
float4x4 WorldViewProjection;

struct VertexShaderInput
{
    float4 Position			: POSITION0;
	float3 Normal           : NORMAL0;
    float4 TextureCoords    : TEXCOORD0;
};

struct HI_VertexShaderInput
{
	float4x4 instanceWorld  : BLENDWEIGHT;
    float4 Color			: TEXCOORD1;
};

struct VertexShaderOutput
{
    float4 Position			: POSITION0;
	float3 Normal	        : TEXCOORD0;
	float2 TextureCoords    : TEXCOORD1;
	float4 Color			: COLOR;
};

VertexShaderOutput Basic_VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
	
	float4 pos = input.Position;
	pos = mul(pos, World);
	pos = mul(pos, WorldViewProjection);

    output.Position = pos;
	output.Normal = input.Normal;
	output.TextureCoords = input.TextureCoords;
	output.Color = float4(1, 0, 0, 1);

    return output;
}

VertexShaderOutput HI_VertexShaderFunction(VertexShaderInput input, HI_VertexShaderInput hi_input)
{
    VertexShaderOutput output;
	
	float4 pos = input.Position;
	pos = mul(pos, transpose(hi_input.instanceWorld));
	pos = mul(pos, WorldViewProjection);

    output.Position = pos;
	output.Normal = input.Normal;
	output.TextureCoords = input.TextureCoords;
	output.Color = hi_input.Color;

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	return input.Color * input.TextureCoords.x;
}

technique Default
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 Basic_VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}


technique HI
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 HI_VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
